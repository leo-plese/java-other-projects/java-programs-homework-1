package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * Test class <code>FactorialTest</code> test functionality of <code>calculateFactorial</code> method from <code>Factorial</code> class.
 * The test cases cover both numbers from allowed and unallowed range.
 * 
 * @author Leo Plese
 * @version 1.0
 */
public class FactorialTest {

	/**
	 * Test for <code>Factorial.calculateFactorial</code> method given the input of 0 which is not from allowed range for the tested method.
	 * The tested method must throw <code>IllegalArgumentException</code>, otherwise the test fails.
	 */
	@Test
	public void numberZeroOutOfRangeCalculateFactorial() {
		try {
			long f = Factorial.calculateFactorial(0);
			fail();
		} catch (IllegalArgumentException ex) {
		}
	}
	
	/**
	 * Test for <code>Factorial.calculateFactorial</code> method given the input of 1 which is not from allowed range for the tested method.
	 * The tested method must throw <code>IllegalArgumentException</code>, otherwise the test fails.
	 */
	@Test
	public void numberTwoOutOfRangeCalculateFactorial() {
		try {
			long f = Factorial.calculateFactorial(2);
			fail();
		} catch (IllegalArgumentException ex) {
		}
	}
	
	/**
	 * Test for <code>Factorial.calculateFactorial</code> method given the input of 21 which is not from allowed range for the tested method.
	 * The tested method must throw <code>IllegalArgumentException</code>, otherwise the test fails.
	 */
	@Test
	public void numberTwentyOneOutOfRangeCalculateFactorial() {
		try {
			long f = Factorial.calculateFactorial(21);
			fail();
		} catch (IllegalArgumentException ex) {
		}
	}

	/**
	 * Test for <code>Factorial.calculateFactorial</code> method given a few inputs which are from allowed range for the tested method.
	 * The tested method must result in correct values, otherwise the test fails.
	 */
	@Test
	public void numberFromRangeCalculateFactorial() {
		assertEquals(6L, Factorial.calculateFactorial(3));
		assertEquals(24L, Factorial.calculateFactorial(4));
		assertEquals(3628800L, Factorial.calculateFactorial(10));
		assertEquals(6402373705728000L, Factorial.calculateFactorial(18));
		assertEquals(2432902008176640000L, Factorial.calculateFactorial(20));
	}
}
