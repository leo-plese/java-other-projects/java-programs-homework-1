package hr.fer.zemris.java.hw01;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.hw01.UniqueNumbers.TreeNode;

/**
 * Test class <code>UniqueNumbersTest</code> test functionality of <code>addNode</code>, <code>treeSize</code>, <code>containsValue</code>, <code>getNodesAscending</code> and <code>getNodesDescendingFromAscending</code> methods
 * from  <code>UniqueNumbers</code> class.
 * 
 * The test cases cover specific cases of a tree (empty tree, one element in a tree, more elements in a tree - two subcases: when there are duplicate values in input and where there are all unique elements in input).
 * In case of any input, the resulting tree has to have unique numbers.
 * 
 * @author Leo Plese
 * @version 1.0
 */
public class UniqueNumbersTest {

	/**
	 * tree's head node used in tests for further tree creation
	 */
	private TreeNode node;

	/**
	 * tree's head node must be set to <code>null</code> in order to have an empty tree at the beginning
	 */
	@BeforeEach
	public void initializeHeadNode() {
		node = null;
	}


	
	/**
	 * Test for <code>UniqueNumbers.addNode</code> method given one input element.
	 * The result must be the tree with one node with value of the input value.
	 */
	@Test
	public void oneElementAddedToTree() {
		node = UniqueNumbers.addNode(node, 50);
		assertEquals(50, node.value);
		assertNull(node.left);
		assertNull(node.right);
	}

	/**
	 * Test for <code>UniqueNumbers.addNode</code> method given more than one input element with some values trying to add more than once.
	 * The result must be the tree with as many nodes as there are different input values (duplicates must not be added).
	 */
	@Test
	public void filledTreeWithInputDuplicatesAddedToTree() {
		setNodeWithDuplicates();

		assertNodeValuesAsExpected();
	}

	/**
	 * Test for <code>UniqueNumbers.addNode</code> method given more than one input element with each value added only once.
	 * The result must be the tree with as many nodes as there are different input values (duplicates must not be added).
	 */
	@Test
	public void filledTreeWithoutInputDuplicatesAddedToTree() {
		setNodeWithoutDuplicates();

		assertNodeValuesAsExpected();
	}

	/**
	 * Method called after filling the tree with more than one value in <code>filledTreeWithInputDuplicatesAddedToTree</code> and <code>filledTreeWithoutInputDuplicatesAddedToTree</code> to assert the statements which must be true.
	 * Specifically, all the correct values added to the tree must be present in it at their particular place.
	 */
	private void assertNodeValuesAsExpected() {
		assertEquals(50, node.value);
		assertEquals(40, node.left.value);
		assertEquals(10, node.left.left.value);
		assertEquals(5, node.left.left.left.value);
		assertEquals(20, node.left.left.right.value);
		assertEquals(60, node.right.value);
		assertEquals(55, node.right.left.value);
		assertEquals(90, node.right.right.value);
		assertEquals(100, node.right.right.right.value);
	}


	
	/**
	 * Test for <code>UniqueNumbers.treeSize</code> method given an empty tree (0 nodes added to the tree).
	 * The result must be the tree with tree size of 0.
	 */
	@Test
	public void emptyTreeSize() {
		int size = UniqueNumbers.treeSize(node);
		assertEquals(0, size);
	}

	/**
	 * Test for <code>UniqueNumbers.treeSize</code> method given a one-node tree (1 node added to the tree).
	 * The result must be the tree with tree size of 1.
	 */
	@Test
	public void oneElementInTreeSize() {
		node = UniqueNumbers.addNode(node, 50);
		int size = UniqueNumbers.treeSize(node);
		assertEquals(1, size);
	}

	/**
	 * Test for <code>UniqueNumbers.treeSize</code> method a given a tree with more than one node with some values trying to add more than once.
	 * The result must be the tree with tree size equal to the number of different numbers in its nodes.
	 */
	@Test
	public void filledTreeWithInputDuplicatesSize() {
		setNodeWithDuplicates();

		int size = UniqueNumbers.treeSize(node);
		assertEquals(9, size);
	}

	/**
	 * Test for <code>UniqueNumbers.treeSize</code> method a given a tree with more than one node with each value added only once.
	 * The result must be the tree with tree size equal to the number of different numbers in its nodes.
	 */
	@Test
	public void filledTreeWithoutInputDuplicatesSize() {
		setNodeWithoutDuplicates();

		int size = UniqueNumbers.treeSize(node);
		assertEquals(9, size);
	}


	
	/**
	 * Test for <code>UniqueNumbers.containsValue</code> method given an empty tree. 
	 * The result must be false because no elements are added to the tree.
	 */
	@Test
	public void emptyTreeContainsValue() {
		assertFalse(UniqueNumbers.containsValue(node, 50));
	}

	/**
	 * Test for <code>UniqueNumbers.containsValue</code> method given a one-node tree. 
	 * The result must be true for value contained in the tree, otherwise false.
	 */
	@Test
	public void oneElementInTreeContainsValue() {
		node = UniqueNumbers.addNode(node, 50);
		assertTrue(UniqueNumbers.containsValue(node, 50));
		assertFalse(UniqueNumbers.containsValue(node, 150));
	}

	/**
	 * Test for <code>UniqueNumbers.containsValue</code> method given a tree with more than one node with some values trying to add more than once. 
	 * The result must be true for values contained in the tree, otherwise false.
	 */
	@Test
	public void filledTreeWithInputDuplicatesContainsValue() {
		setNodeWithDuplicates();

		assertContainsValues();
	}

	/**
	 * Test for <code>UniqueNumbers.containsValue</code> method given a tree with more than one node with each value added only once.
	 * The result must be true for values contained in the tree, otherwise false.
	 */
	@Test
	public void filledTreeWithoutInputDuplicatesContainsValue() {
		setNodeWithoutDuplicates();

		assertContainsValues();
	}

	/**
	 * Method called after filling the tree with more than one value in <code>filledTreeWithInputDuplicatesContainsValue</code> and <code>filledTreeWithoutInputDuplicatesContainsValue</code> to assert the statements which must be true.
	 * Specifically, all the values added to the tree must be present in it at their particular place while all the others must not be there.
	 */
	private void assertContainsValues() {
		assertTrue(UniqueNumbers.containsValue(node, 5));
		assertTrue(UniqueNumbers.containsValue(node, 10));
		assertTrue(UniqueNumbers.containsValue(node, 20));
		assertTrue(UniqueNumbers.containsValue(node, 40));
		assertTrue(UniqueNumbers.containsValue(node, 50));
		assertTrue(UniqueNumbers.containsValue(node, 55));
		assertTrue(UniqueNumbers.containsValue(node, 60));
		assertTrue(UniqueNumbers.containsValue(node, 90));
		assertTrue(UniqueNumbers.containsValue(node, 100));
		
		assertFalse(UniqueNumbers.containsValue(node, 1));
		assertFalse(UniqueNumbers.containsValue(node, 35));
		assertFalse(UniqueNumbers.containsValue(node, 51));
		assertFalse(UniqueNumbers.containsValue(node, 150));
	}
	
	
	
	/**
	 * Test for <code>UniqueNumbers.getNodesAscending</code> method given an empty tree. 
	 * The result must be an empty string representing the empty tree.
	 */
	@Test
	public void emptyTreeGetNodesAscending() {
		String nodesAscending = UniqueNumbers.getNodesAscending(node, "");
		assertEquals("", nodesAscending);
	}

	/**
	 * Test for <code>UniqueNumbers.getNodesAscending</code> method given a one-node tree. 
	 * The result must be a string containing that very value.
	 */
	@Test
	public void oneElementInTreeGetNodesAscending() {
		node = UniqueNumbers.addNode(node, 50);
		String nodesAscending = UniqueNumbers.getNodesAscending(node, "");
		assertEquals(" 50", nodesAscending);
	}
	
	/**
	 * Test for <code>UniqueNumbers.getNodesAscending</code> method given a tree with more than one node with some values trying to add more than once. 
	 * The result must be a string containing the unique values in ascending order.
	 */
	@Test
	public void filledTreeWithInputDuplicatesGetNodesAscending() {
		setNodeWithDuplicates();
		String nodesAscending = UniqueNumbers.getNodesAscending(node, "");
		assertEquals(" 5 10 20 40 50 55 60 90 100", nodesAscending);
	}
	
	/**
	 * Test for <code>UniqueNumbers.getNodesAscending</code> method given a tree with each value added only once.
	 * The result must be a string containing the unique values in ascending order.
	 */
	@Test
	public void filledTreeWithoutInputDuplicatesGetNodesAscending() {
		setNodeWithoutDuplicates();
		String nodesAscending = UniqueNumbers.getNodesAscending(node, "");
		assertEquals(" 5 10 20 40 50 55 60 90 100", nodesAscending);
	}
	
	

	/**
	 * Test for <code>UniqueNumbers.getNodesDescending</code> method given an empty tree. 
	 * The result must be an empty string representing the empty tree.
	 */
	@Test
	public void emptyTreeGetNodesDescendingFromAscending() {
		String nodesDescending = getNodesDescending();
		assertEquals("", nodesDescending);
	}

	/**
	 * Test for <code>UniqueNumbers.getNodesDescending</code> method given given a one-node tree.
	 * The result must be a string containing that very value.
	 */
	@Test
	public void oneElementInTreeGetNodesDescendingFromAscending() {
		node = UniqueNumbers.addNode(node, 50);
		String nodesDescending = getNodesDescending();
		assertEquals(" 50", nodesDescending);
	}
	
	/**
	 * Test for <code>UniqueNumbers.getNodesDescending</code> method given a tree with more than one node with some values trying to add more than once.
	 * The result must be a string containing the unique values in descending order.
	 */
	@Test
	public void filledTreeWithInputDuplicatesGetNodesDescendingFromAscending() {
		setNodeWithDuplicates();
		String nodesDescending = getNodesDescending();
		assertEquals(" 100 90 60 55 50 40 20 10 5", nodesDescending);
	}
	
	/**
	 * Test for <code>UniqueNumbers.getNodesDescending</code> method given a tree with more than one node with each value added only once.
	 * The result must be a string containing the unique values in descending order.
	 */
	@Test
	public void filledTreeWithoutInputDuplicatesGetNodesDescendingFromAscending() {
		setNodeWithoutDuplicates();
		String nodesDescending = getNodesDescending();
		assertEquals(" 100 90 60 55 50 40 20 10 5", nodesDescending);
	}
	
	/**
	 * Method called after filling the tree with more than one value in <code>filledTreeWithInputDuplicatesGetNodesDescendingFromAscending</code> and <code>filledTreeWithoutInputDuplicatesGetNodesDescendingFromAscending</code> to get the string
	 * of node values in descending order from the string representing the values in ascending order therefore calling both <code>getNodesAscending</code> and <code>getNodesDescendingFromAscending</code>.
	 * 
	 * @return string representing values from tree nodes in descending order
	 */
	private String getNodesDescending() {
		return UniqueNumbers.getNodesDescendingFromAscending(UniqueNumbers.getNodesAscending(node, ""));
	}
	
	
	
	/**
	 * Method called to set tree nodes to particular values with some values trying to add more than once.
	 * The result must be, however, a tree with all the values appearing only once.
	 */
	private void setNodeWithDuplicates() {
		setNodeWithoutDuplicates();

		node = UniqueNumbers.addNode(node, 50);
		node = UniqueNumbers.addNode(node, 10);
		node = UniqueNumbers.addNode(node, 90);
	}

	/**
	 * Method called to set tree nodes to particular values with each value added only once.
	 * The result is a tree with all the values appearing only once.
	 */
	private void setNodeWithoutDuplicates() {
		node = UniqueNumbers.addNode(node, 50);
		node = UniqueNumbers.addNode(node, 40);
		node = UniqueNumbers.addNode(node, 60);
		node = UniqueNumbers.addNode(node, 10);
		node = UniqueNumbers.addNode(node, 5);
		node = UniqueNumbers.addNode(node, 90);
		node = UniqueNumbers.addNode(node, 100);
		node = UniqueNumbers.addNode(node, 20);
		node = UniqueNumbers.addNode(node, 55);
	}
}
