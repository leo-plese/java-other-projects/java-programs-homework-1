package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Class <code>Rectangle</code> enables calculation of a rectangle area and circumference.
 * User inputs via standard input or direct input parameters to main program rectangle dimensions (width and height)
 * 
 * @author Leo Plese
 * @version 1.0
 */
public class Rectangle {
	
	/**
	 * Values of rectangle dimensions - width and height (real numbers).
	 */
	private double width, height;
	
	/**
	 * Main method calling additional methods to correctly interpret user input given over console or input arguments and finally calculate rectangle area and circumference. 
	 * 
	 * @param args two input arguments - rectangle width and height respectively (real numbers) 
	 */
	public static void main(String ... args) {
		if (args.length != 0 && args.length != 2) {
			System.out.println("Nije unesen dozvoljen broj argumenata!");
			System.exit(0);
		}
		
		Rectangle rectangle = new Rectangle();
		
		if (args.length == 2) {
			try {
				rectangle.width = getRectangleDimension(args[0]);
				rectangle.height = getRectangleDimension(args[1]);
			} catch (IllegalArgumentException ex) {
				System.out.println(ex.getMessage());
				return;
			}
			
		} else {
			Scanner sc = new Scanner(System.in);
		
			rectangle.width = enterRectangleDimension(sc, "širinu");
			rectangle.height = enterRectangleDimension(sc, "visinu");
			
			sc.close();
		}
		
		System.out.format("Pravokutnik širine %.1f i visine %.1f ima površinu %.1f te opseg %.1f.%n", rectangle.width, rectangle.height, calculateArea(rectangle.width, rectangle.height), calculateCircumference(rectangle.width, rectangle.height));
	}
	
	/**
	 * Method parsing input string into a real number (under the restriction set to it).
	 * The method is specifically adjusted to and used in the case of user input via input arguments to the program.
	 * 
	 * Important restriction: <code>dimension</code> has to be a positive real number
	 * 
	 * @param dimensionEntry string representing width or height of a rectangle which is to be parsed
	 * @return real number representing value of the input rectangle dimension
	 * @throws NumberFormatException thrown if the input string representation of a rectangle width/height cannot be interpreted as a real number
	 * @throws IllegalArgumentException thrown if the input is not from allowed range (positive number)
	 */
	private static double getRectangleDimension(String dimensionEntry) throws NumberFormatException, IllegalArgumentException {
		double dimension = 0;
		
		try {
			dimension = getDimensionValue(dimensionEntry);
		} catch (NumberFormatException ex) {
			throw new NumberFormatException("'" + dimensionEntry + "' se ne može protumačiti kao broj.");
		}
		
		if (!positiveValueCheck(dimension)) {
			throw new IllegalArgumentException("Unijeli ste negativnu vrijednost.");
		}
		
		return dimension;
	}
	
	/**
	 * Method parsing input string into a real number (under the restriction set to it).
	 * The method is specifically adjusted to and used in the case of user input via console.
	 * 
	 * Important restriction: <code>dimension</code> has to be a positive real number
	 * 
	 * @param sc <code>Scanner</code> object used for user console interaction
	 * @param dimensionName String representing the name of a rectangle dimension (e.g. "širinu", "visinu")
	 * @return real number representing value of the input rectangle dimension
	 */
	private static double enterRectangleDimension(Scanner sc, String dimensionName) {
		while (true) {
			System.out.print("Unesite " + dimensionName + " > ");
			String dimensionEntry = sc.next();
			
			double dimension;
			
			try {
				dimension = getDimensionValue(dimensionEntry);
			} catch (NumberFormatException ex) {
				printInvalidEntryMessage(dimensionEntry);
				continue;
			}
			
			if (!positiveValueCheck(dimension)) {
				printInvalidNumberRangeMessage();
				continue;
			}

			return dimension;
		}
	}
	
	/**
	 * Method called in <code>enterRectangleDimension</code> to try to parse the input string as a real number.
	 * The method is specifically adjusted to and used in the case of user input via console.
	 * 
	 * @param entry string representing to be parsed into a valid rectangle dimension value
	 * @return dimension value (real number)
	 * @throws NumberFormatException thrown if the parsing was not successful
	 */
	private static double getDimensionValue(String entry) throws NumberFormatException {
		return Double.parseDouble(entry);
	}
	
	/**
	 * Method called in <code>enterRectangleDimension</code> and <code>getRectangleDimension</code> to check if it is positive.
	 * 
	 * @param value real number to be checked whether it is positive (dimension must not be a negative number or zero)
	 * @return boolean telling if value is positive
	 */
	private static boolean positiveValueCheck(double value) {
		return value > 0;
	}
	
	/**
	 * Method called in <code>enterRectangleDimension</code> to print out the suitable message if the input cannot be interpreted as a number.
	 * 
	 * @param entry entry which appears in the message
	 */
	private static void printInvalidEntryMessage(String entry) {
		System.out.println("'" + entry + "' se ne može protumačiti kao broj.");
	}
	
	/**
	 *  Method called in <code>enterRectangleDimension</code> to print out the suitable message if the input is negative or zero (not from allowed range).
	 */
	private static void printInvalidNumberRangeMessage() {
		//this message pertains both to negative numbers and zero which are both out of the allowed range of positive numbers
		System.out.println("Unijeli ste negativnu vrijednost.");
	}
	
	/**
	 * Method calculating area of a rectangle.
	 * 
	 * @param width rectangle width value (real positive number)
	 * @param height rectangle height value (real positive number)
	 * @return rectangle area (real positive number) calculated using geometry rectangle area formula: area = width * height
	 */
	private static double calculateArea(double width, double height) {
		return width * height;
	}
	
	/**
	 * Method calculating circumference of a rectangle.
	 * 
	 * @param width rectangle width value (real positive number)
	 * @param height rectangle height value (real positive number)
	 * @return rectangle circumference (real positive number) calculated using geometry rectangle area formula: area = 2 * (width + height)
	 */
	private static double calculateCircumference(double width, double height) {
		return 2 * (width + height);
	}
}
