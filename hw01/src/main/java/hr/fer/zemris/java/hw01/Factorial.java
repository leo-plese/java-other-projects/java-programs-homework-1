package hr.fer.zemris.java.hw01;

import java.util.Scanner;


/**
 * Class <code>Factorial</code> enables calculation of factorials for natural numbers from 3 to 20.
 * User inputs via standard input a number whose factorial is to be calculated through standard input and final result is then calculated (if possible) and printed out on the screen.
 * 
 * @author Leo Plese
 * @version 1.0
 */
public class Factorial {
	
	/**
	 * Main method calling additional methods to realize user interaction so as to get the user input and finally calculate its factorial.
	 * 
	 * @param args input arguments not used in this program
	 */
	public static void main(String ... args ) {
		Scanner sc = new Scanner(System.in);
		
		while (true) {
			System.out.print("Unesite broj > ");
			String entry = sc.next();
					
			if (entry.equals("kraj"))
				break;
			
			int number;
			
			try {
				number = Integer.parseInt(entry);
			} catch (NumberFormatException ex) {
				System.out.println("'" + entry + "' nije cijeli broj.");
				continue;
			}
			
			
			try {
				long factorial = calculateFactorial(number);
				System.out.format("%d! = %d%n", number, factorial);
			} catch (IllegalArgumentException ex) {
				System.out.println("'" + number + "' nije broj u dozvoljenom rasponu.");
			}
		}
		
		System.out.println("Doviđenja.");
		
		sc.close();
	}
	
	/**
	 * Method calculating factorial of the input number.
	 * 
	 * Important restriction: <code>number</code> has to be a natural number number between 3 and 20 inclusive
	 * 
	 * @param number number whose factorial is to be calculated
	 * @return factorial of the input number
	 * @throws IllegalArgumentException thrown if number is not from the allowed range (natural number between 3 and 20 inclusive)
	 */
	public static long calculateFactorial(int number) throws IllegalArgumentException {
		if (number < 3 || number > 20)
			throw new IllegalArgumentException();
		
		long factorial = 1;
		
		for (int i = 2; i <= number; i++) {
			factorial *= i;
		}
		
		return factorial;
	}
}
