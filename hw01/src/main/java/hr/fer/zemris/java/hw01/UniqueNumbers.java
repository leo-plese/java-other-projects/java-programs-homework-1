package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * Class <code>UniqueNumbers</code> enables creation of a sorted binary tree with unique elements (cannot have duplicate tree node values).
 * Also, the class provides information about its size and check of particular elements' presence.
 * 
 * @author Leo Plese
 * @version 1.0
 */
public class UniqueNumbers {

	/**
	 * Class <code>TreeNode</code> representing model of a tree node which has its <code>value</code> and two children - <code>left</code> and <code>right</code>.
	 */
	static class TreeNode {
		TreeNode left;
		TreeNode right;
		int value;
	}

	/**
	 * Main method calling additional methods to realize user interaction so as to get the user input and add the input values to the tree under the restriction that elements have to be unique.
	 * Finally, it provides the output listing the tree node values in ascending and descending order.
	 * 
	 * @param args input arguments not used in this program
	 */
	public static void main(String... args) {
		TreeNode head = null;

		Scanner sc = new Scanner(System.in);

		head = enterNodeValues(sc, head);

		String valuesAscending = getNodesAscending(head, "");
		System.out.print("Ispis od najmanjeg:" + valuesAscending);

		System.out.println();

		String valuesDescending = getNodesDescendingFromAscending(valuesAscending);
		System.out.print("Ispis od najvećeg:" + valuesDescending);

		sc.close();
	}
	
	/**
	 * Method enables user to input values to be added to the tree.
	 * 
	 * Important restrictions (ensured by calling appropriate methods):
	 * - the user-input values must be of type specified in <code>TreeNode</code> (<code>int</code>)
	 * - the same input must not be added to the tree as a new node more than ones
	 * 
	 * @param sc <code>Scanner</code> object used for user console interaction
	 * @param head tree's head node further elements are added to
	 * @return head node after adding all the input nodes with values from user input
	 */
	private static TreeNode enterNodeValues(Scanner sc, TreeNode head) {
		while (true) {
			System.out.print("Unesite broj > ");
			String entry = sc.next();

			if (entry.equals("kraj"))
				break;

			int value;

			try {
				value = Integer.parseInt(entry);
			} catch (NumberFormatException ex) {
				System.out.println("'" + entry + "' nije cijeli broj.");
				continue;
			}

			if (containsValue(head, value)) {
				System.out.println("Broj već postoji. Preskačem.");
				continue;
			}
			
			head = addNode(head, value);
			System.out.println("Dodano.");
		}
		
		return head;
	}

	/**
	 * Method creating string containing tree node values in ascending order.
	 * Based on the recursive algorithm.
	 * 
	 * @param node tree's head node
	 * @param message input string further values are concatenated to (to be set to an empty string in method call)
	 * @return string representing values of tree nodes in ascending order
	 */
	public static String getNodesAscending(TreeNode node, String message) {
		if (node == null) {
			return "";
		}

		return getNodesAscending(node.left, message) + " " + node.value + getNodesAscending(node.right, message);
	}

	/**
	 * Method creating string containing tree node values in ascending order.
	 * The algorithm is based on the input string <code>valuesAscending</code> being reversed to get the opposite order of tree node values (from ascending to descending).
	 * 
	 * @param valuesAscending string representing values of tree nodes in ascending order
	 * @return string representing values of tree nodes in descending order
	 */
	public static String getNodesDescendingFromAscending(String valuesAscending) {
		String[] valuesStr = valuesAscending.split(" ");

		String valuesDescending = "";
		for (int i = valuesStr.length - 1; i >= 0; i--) {
			String s = valuesStr[i].trim();
			if (s.isEmpty()) {
				continue;
			}
			valuesDescending += " " + s;
		}

		return valuesDescending;
	}

	/**
	 * Method adding a new node to the tree if the value is not already present in it.
	 * Based on the recursive algorithm.
	 * 
	 * Important: the new node is being added as in a sorted binary tree (nodes with value less than their parent's go to the parent's left, otherwise to the right)
	 * 
	 * @param node tree's head node
	 * @param value value to be added to a new node to the tree
	 * @return tree node added to the tree
	 */
	public static TreeNode addNode(TreeNode node, int value) {
		if (node == null) {
			node = new TreeNode();
			node.value = value;
			return node;
		}

		if (node.value == value) {
			return node;
		}

		if (value < node.value) {
			node.left = addNode(node.left, value);
		} else {
			node.right = addNode(node.right, value);
		}

		return node;
	}

	/**
	 * Method calculating a tree's size i.e. counting the number of nodes in the tree.
	 * Based on the recursive algorithm.
	 * 
	 * @param node tree's head node
	 * @return size of a tree (number of nodes in the tree)
	 */
	public static int treeSize(TreeNode node) {
		if (node == null) {
			return 0;
		}

		return 1 + treeSize(node.left) + treeSize(node.right);
	}

	/**
	 * Method checking if a particular value is present in any node of a given tree.
	 * It is used to determined whether the restriction of unique numbers in the tree is fulfilled.
	 * Based on the recursive algorithm.
	 * 
	 * @param node tree's head node
	 * @param value value whose presence in a given tree is to be tested
	 * @return true if the tree already contains value
	 */
	public static boolean containsValue(TreeNode node, int value) {
		if (node == null) {
			return false;
		}

		return node.value == value || containsValue(node.left, value) || containsValue(node.right, value);
	}

}
